package is.symphony.test.workshop.microservice.showcase.sheetsservice.repository;

import is.symphony.test.workshop.microservice.showcase.sheetsservice.model.SheetDataDB;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface SheetDataRepository extends ReactiveCassandraRepository<SheetDataDB, UUID> {
    @Query("SELECT * FROM " + SheetDataDB.TABLE_NAME + " WHERE " + SheetDataDB.THREAD_ID + "=?0 AND " + SheetDataDB.ID + "=?1")
    Mono<SheetDataDB> findByThreadIdAndId(String threadId, UUID id);

    @Query("SELECT * FROM " + SheetDataDB.TABLE_NAME + " WHERE " + SheetDataDB.THREAD_ID + "=?0")
    Flux<SheetDataDB> findAllByThreadId(String threadId);
}
