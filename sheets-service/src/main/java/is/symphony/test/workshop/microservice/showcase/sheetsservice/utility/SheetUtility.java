package is.symphony.test.workshop.microservice.showcase.sheetsservice.utility;

import is.symphony.test.workshop.microservice.showcase.generated.model.RowData;
import is.symphony.test.workshop.microservice.showcase.generated.model.SheetData;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.model.SheetDataDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.*;
import java.util.stream.Collectors;

public class SheetUtility {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetUtility.class);

    private SheetUtility() {}

    public static Flux<SheetDataDB> convertSheetDataToSheetSongDB(SheetData sheetData, List<SheetDataDB> existingSheetDataList) {

        Map<Integer, SheetDataDB> stored = getMapForFilteringExistingSheetSongs(existingSheetDataList);

        List<SheetDataDB> sheetDataDBList =
                new ArrayList<>(sheetData.getSheets().get(0).getData().get(0).getRowData().size());

        for (int i = 1; i < sheetData.getSheets().get(0).getData().get(0).getRowData().size(); i++) {
            if (stored.get(i) != null &&
                    stored.get(i).getStatus() != null &&
                    !stored.get(i).getStatus().isBlank()) continue;


            RowData rowData = sheetData.getSheets().get(0).getData().get(0).getRowData().get(i);
            LOGGER.info("rowData: {}", rowData.getValues());    // TODO: DELETE
            String songId = rowData.getValues().get(0).getFormattedValue();
            String songName = rowData.getValues().size() > 1 ? rowData.getValues().get(1).getFormattedValue() : null;

            sheetDataDBList.add(new SheetDataDB()
                    .setId(stored.get(i) == null ? UUID.randomUUID() : stored.get(i).getId())
                    .setThreadId(sheetData.getSpreadsheetId())
                    .setRowNumber(i)
                    .setSongId(songId == null || songId.isBlank() ? null : songId)
                    .setSongName(songName));
        }

        LOGGER.info("Extracted {} rows from sheet {}!", sheetDataDBList.size(), sheetData.getSpreadsheetId());

        return Flux.fromIterable(sheetDataDBList);
    }

    private static Map<Integer, SheetDataDB> getMapForFilteringExistingSheetSongs(List<SheetDataDB> existingSheetDataList) {
        return existingSheetDataList
                .stream()
                .filter(e -> e.getStatus() != null && !e.getStatus().isBlank())
                .collect(Collectors.toMap(SheetDataDB::getRowNumber, e -> e));
    }
}
