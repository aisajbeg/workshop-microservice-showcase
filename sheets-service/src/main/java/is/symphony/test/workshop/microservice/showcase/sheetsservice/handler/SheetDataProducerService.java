package is.symphony.test.workshop.microservice.showcase.sheetsservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetData;
import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.properties.SheetsProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.SenderResult;

public class SheetDataProducerService {
    private final Logger LOGGER = LoggerFactory.getLogger(SheetDataProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, SheetSong> reactiveKafkaProducerTemplate;
    private final SheetsProperties sheetsProperties;

    public SheetDataProducerService(ReactiveKafkaProducerTemplate<String, SheetSong> reactiveKafkaProducerTemplate, SheetsProperties sheetsProperties) {
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
        this.sheetsProperties = sheetsProperties;
    }

    public Mono<SenderResult<Void>> send(SheetSong sheetSong) {
        LOGGER.info("send to topic={}, {}={},", sheetsProperties.getSheetDataKafkaTopic(), SheetData.class.getSimpleName(), sheetSong);

        return reactiveKafkaProducerTemplate.send(sheetsProperties.getSheetDataKafkaTopic(), sheetSong)
                .doOnSuccess(senderResult -> LOGGER.info("sent {} offset : {}", sheetSong, senderResult.recordMetadata().offset()));
    }
}
