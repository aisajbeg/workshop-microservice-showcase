package is.symphony.test.workshop.microservice.showcase.sheetsservice.serde;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetData;
import org.springframework.kafka.support.serializer.JsonDeserializer;

public class SheetDataDeserializer extends JsonDeserializer<SheetData> {
}
