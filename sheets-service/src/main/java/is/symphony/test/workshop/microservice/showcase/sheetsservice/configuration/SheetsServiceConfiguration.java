package is.symphony.test.workshop.microservice.showcase.sheetsservice.configuration;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.handler.SheetDataProducerService;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.properties.SheetsProperties;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.repository.SheetDataRepository;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.service.SheetsService;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.utility.SheetUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableScheduling
@ComponentScan({
        "is.symphony.test.workshop.microservice.showcase.sheetsservice.properties",
        "is.symphony.test.workshop.microservice.showcase.sheetsservice.handler",
        "is.symphony.test.workshop.microservice.showcase.sheetsservice.service"
})
public class SheetsServiceConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetsServiceConfiguration.class);


    private SheetsProperties sheetsProperties;

    private WebClient.Builder webClientBuilder;
    private SheetDataRepository sheetDataRepository;

    private final ReactiveKafkaProducerTemplate<String, SheetSong> reactiveKafkaProducerTemplate;

    @Value("${sheets.id}")
    private String id;

    public SheetsServiceConfiguration(SheetsProperties sheetsProperties,
                                      WebClient.Builder webClientBuilder,
                                      SheetDataRepository sheetDataRepository,
                                      ReactiveKafkaProducerTemplate<String, SheetSong> reactiveKafkaProducerTemplate) {
        this.sheetsProperties = sheetsProperties;
        this.webClientBuilder = webClientBuilder;
        this.sheetDataRepository = sheetDataRepository;
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    @Scheduled(cron = "${sheets.pulling:-}")
    public void getData() {
        LOGGER.info("Pulling data from sheet and sending it to kafka...");

        sheetsService()
                .getData()
                .zipWith(sheetDataRepository.findAllByThreadId(sheetsProperties.getId()).collectList())
                .flatMapMany(zip -> SheetUtility.convertSheetDataToSheetSongDB(zip.getT1(), zip.getT2()))
                .flatMap(e -> sheetDataRepository.save(e)
                        .flatMap(sheetDataDB -> sheetDataReactiveProducerService().send(sheetDataDB.getDTO()))
                )
                .doOnError(e -> LOGGER.error("Issue!", e))
                .subscribe();
    }

    @Bean
    public SheetDataProducerService sheetDataReactiveProducerService() {
        return new SheetDataProducerService(reactiveKafkaProducerTemplate, sheetsProperties);
    }

    @Bean
    public WebClient sheetsClient() {
        if(sheetsProperties.getApiKey() == null || sheetsProperties.getUrl() == null || sheetsProperties.getId() == null) {
            LOGGER.info("something is null... {}, {}, {}", sheetsProperties.getApiKey(), sheetsProperties.getUrl(), sheetsProperties.getId());
            return null;
        }

        return webClientBuilder
                .baseUrl(sheetsProperties.getUrl())
                .defaultHeaders(header -> header.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .build();
    }

    @Bean
    public SheetsService sheetsService() {
        return new SheetsService(sheetsClient(), sheetsProperties, sheetDataRepository);
    }
}
