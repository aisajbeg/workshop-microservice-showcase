package is.symphony.test.workshop.microservice.showcase.sheetsservice.service;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetData;
import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.model.SheetDataDB;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.properties.SheetsProperties;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.repository.SheetDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class SheetsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetsService.class);

    private final WebClient sheetsClient;
    private final SheetsProperties sheetsProperties;
    private final SheetDataRepository sheetDataRepository;

    public SheetsService(final WebClient sheetsClient,
                         final SheetsProperties sheetsProperties,
                         final SheetDataRepository sheetDataRepository) {
        this.sheetsClient = sheetsClient;
        this.sheetsProperties = sheetsProperties;
        this.sheetDataRepository = sheetDataRepository;
    }

    public Mono<SheetData> getData() {
        LOGGER.info("Retrieving data from google sheets {}...", sheetsProperties.getId());
        return sheetsClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(sheetsProperties.getId())
                        .queryParam("key", sheetsProperties.getApiKey())
                        .queryParam("includeGridData", true)
                        .build())
                .retrieve()
                .bodyToMono(SheetData.class)
                .map(e -> {
                    LOGGER.info("Response: {}", e);
                    return e;
                });

    }

    public Mono<Void> updateSheetSong(SheetSong sheetSong) {
        return sheetDataRepository.findByThreadIdAndId(sheetSong.getSpreadsheetId(), sheetSong.getId())
                .flatMap(e -> sheetDataRepository
                        .save(e.setShowId(sheetSong.getShowId())
                                .setStatus(sheetSong.getStatus())
                        )
                )
                .doOnNext(this::updateSheetValues)
                .then();
    }

    private Mono<Void> updateSheetValues(SheetDataDB sheetDataDB) {
        return Mono.empty();
        /**
        return sheetsClient
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(sheetsProperties.getId() + "/values/C" + sheetDataDB.getRowNumber())
                        .queryParam("key", sheetsProperties.getApiKey())
                        .queryParam("includeValuesInResponse", true)
                        .build())
                        .body()
                        .retrieve()
         */
    }

}
