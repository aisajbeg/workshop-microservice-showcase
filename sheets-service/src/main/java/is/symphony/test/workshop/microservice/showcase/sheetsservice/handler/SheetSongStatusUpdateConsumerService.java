package is.symphony.test.workshop.microservice.showcase.sheetsservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.sheetsservice.service.SheetsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class SheetSongStatusUpdateConsumerService {
    private final Logger LOGGER = LoggerFactory.getLogger(SheetSongStatusUpdateConsumerService.class);

    private final SheetsService sheetsService;

    public SheetSongStatusUpdateConsumerService(SheetsService sheetsService) {
        this.sheetsService = sheetsService;
    }

    @KafkaListener(topics = "${task.sheetSongUpdateKafkaTopic}", groupId = "${task.groupName}", containerFactory = "sheetSongUpdateKafkaListenerContainerFactory")
    public void consumeUpdate(SheetSong sheetSong) {
        LOGGER.info("Got update request {}", sheetSong);
        sheetsService.updateSheetSong(sheetSong).subscribe();
    }
}
