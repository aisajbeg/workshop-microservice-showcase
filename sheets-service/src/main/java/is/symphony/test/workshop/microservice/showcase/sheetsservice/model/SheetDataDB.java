package is.symphony.test.workshop.microservice.showcase.sheetsservice.model;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Objects;
import java.util.UUID;

@Table(SheetDataDB.TABLE_NAME)
public class SheetDataDB {
    public static final String TABLE_NAME = "sheet_data";
    public static final String SONG_ID = "song_id";
    public static final String SONG_NAME = "song_name";
    public static final String SHOW_ID = "show_id";
    public static final String ROW_NUMBER = "row_number";
    public static final String STATUS = "status";
    public static final String ID = "id";
    public static final String THREAD_ID = "thread_id";

    @PrimaryKeyColumn(
            name = ID,
            ordinal = 1,
            type = PrimaryKeyType.CLUSTERED,
            ordering = Ordering.DESCENDING)
    private UUID id;
    @PrimaryKeyColumn(
            name = THREAD_ID,
            ordinal = 0,
            type = PrimaryKeyType.PARTITIONED,
            ordering = Ordering.DESCENDING)
    private String threadId;
    @Column(SONG_ID)
    private String songId;
    @Column(SONG_NAME)
    private String songName;
    @Column(ROW_NUMBER)
    private Integer rowNumber;
    @Column(STATUS)
    private String status;
    @Column(SHOW_ID)
    private String showId;

    public String getSongId() {
        return songId;
    }

    public SheetDataDB setSongId(String songId) {
        this.songId = songId;
        return this;
    }

    public String getSongName() {
        return songName;
    }

    public SheetDataDB setSongName(String songName) {
        this.songName = songName;
        return this;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public SheetDataDB setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public SheetDataDB setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getShowId() {
        return showId;
    }

    public SheetDataDB setShowId(String showId) {
        this.showId = showId;
        return this;
    }

    public UUID getId() {
        return id;
    }

    public SheetDataDB setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getThreadId() {
        return threadId;
    }

    public SheetDataDB setThreadId(String threadId) {
        this.threadId = threadId;
        return this;
    }

    public SheetSong getDTO() {
        return new SheetSong()
                .id(this.id)
                .showId(this.showId)
                .songId(this.songId)
                .songName(this.songName)
                .spreadsheetId(this.threadId)
                .status(this.status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SheetDataDB that = (SheetDataDB) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(threadId, that.threadId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, threadId);
    }

    @Override
    public String toString() {
        return "SheetDataDB{" +
                "id=" + id +
                ", threadId='" + threadId + '\'' +
                ", songId='" + songId + '\'' +
                ", songName='" + songName + '\'' +
                ", rowNumber=" + rowNumber +
                ", status='" + status + '\'' +
                ", showId='" + showId + '\'' +
                '}';
    }
}
