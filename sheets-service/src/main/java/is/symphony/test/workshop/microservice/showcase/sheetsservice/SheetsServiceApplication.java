package is.symphony.test.workshop.microservice.showcase.sheetsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class SheetsServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SheetsServiceApplication.class, args);
    }
}
