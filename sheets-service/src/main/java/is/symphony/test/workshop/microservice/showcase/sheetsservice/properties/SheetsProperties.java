package is.symphony.test.workshop.microservice.showcase.sheetsservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("sheets")
public class SheetsProperties {

    private String url;
    private String apiKey;
    private String id;
    private String sheetDataKafkaTopic;

    public String getUrl() {
        return url;
    }

    public SheetsProperties setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getApiKey() {
        return apiKey;
    }

    public SheetsProperties setApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public String getId() {
        return id;
    }

    public SheetsProperties setId(String id) {
        this.id = id;
        return this;
    }

    public String getSheetDataKafkaTopic() {
        return sheetDataKafkaTopic;
    }

    public SheetsProperties setSheetDataKafkaTopic(String sheetDataKafkaTopic) {
        this.sheetDataKafkaTopic = sheetDataKafkaTopic;
        return this;
    }
}
