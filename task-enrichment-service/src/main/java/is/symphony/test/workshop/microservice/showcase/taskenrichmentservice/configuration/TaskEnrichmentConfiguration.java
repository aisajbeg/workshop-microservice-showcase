package is.symphony.test.workshop.microservice.showcase.taskenrichmentservice.configuration;

import org.springframework.web.reactive.function.client.WebClient;


public class TaskEnrichmentConfiguration {


    public static class LyricsWebClientProvider {
        private WebClient webClient;

        public WebClient getWebClient() {
            return webClient;
        }

        public void setWebClient(WebClient webClient) {
            this.webClient = webClient;
        }
    }
}
