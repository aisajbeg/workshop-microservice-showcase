package is.symphony.test.workshop.microservice.showcase.taskenrichmentservice;

import is.symphony.test.workshop.microservice.showcase.cassandra.configuration.CassandraConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableConfigurationProperties
@Import(CassandraConfiguration.class)
public class TaskEnrichmentServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(TaskEnrichmentServiceApplication.class, args);
    }
}
