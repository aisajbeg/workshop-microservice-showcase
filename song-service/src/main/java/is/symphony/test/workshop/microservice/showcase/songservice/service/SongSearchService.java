package is.symphony.test.workshop.microservice.showcase.songservice.service;

import is.symphony.test.workshop.microservice.showcase.generated.model.*;
import is.symphony.test.workshop.microservice.showcase.songservice.configuration.MusicApiConfiguration;
import is.symphony.test.workshop.microservice.showcase.songservice.handler.SongMatchesKafkaProducerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static is.symphony.test.workshop.microservice.showcase.songservice.util.SongUtil.setDefaultIfNone;
import static org.apache.commons.lang.StringUtils.isNumeric;

@Service
public class SongSearchService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SongSearchService.class);

    private final MusicApiConfiguration.MusicApiClientProvider musicApiClientProvider;
    private final SongMatchesKafkaProducerService songMatchesKafkaProducerService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public SongSearchService(final MusicApiConfiguration.MusicApiClientProvider musicApiClientProvider,
                             SongMatchesKafkaProducerService songMatchesKafkaProducerService) {
        this.musicApiClientProvider = musicApiClientProvider;
        this.songMatchesKafkaProducerService = songMatchesKafkaProducerService;
    }


    public Mono<Void> createTask(List<MusicAPISong> musicApiSongList, SheetSong sheetSong) throws ExecutionException, InterruptedException {
        LOGGER.info("sending list of songs to task service to create new task...");

        SongMatches songMatches = new SongMatches()
                .inputSong(sheetSong)
                .matchedSongs(musicApiSongList == null ? Collections.EMPTY_LIST : musicApiSongList);


        return songMatchesKafkaProducerService.send(songMatches).then();
    }
    public Flux<MusicAPISong> searchSongs(String term, String storefront, int fetchSize) {
        return isNumeric(term) ?
                getSongsFromStringListOfAdamIDs(term, storefront) :
                searchSongsByStringParam(term, storefront, fetchSize);
    }

    private Flux<MusicAPISong> getSongsFromStringListOfAdamIDs(String adamIds, String storefront) {
        LOGGER.debug("searching by adam Ids: {}, storefront: {}", adamIds, storefront);

        return musicApiClientProvider.getWebClient().get().uri(uriBuilder -> uriBuilder
                .path("/v1/catalog/" + setDefaultIfNone(storefront) + "/songs")
                .queryParam("ids", adamIds)
                .build()).retrieve().bodyToFlux(ResultData.class)
                .flatMap(response ->
                        Flux.fromIterable(response.getData().stream().map(data -> {
                                    MusicAPISong song = data.getAttributes();
                                    song.setId(data.getId());

                                    return song;
                                }).collect(Collectors.toCollection(ArrayList::new))
                        ));
    }

    public Flux<MusicAPISong> searchSongsByStringParam(String term, String storefront, int fetchLimit) {
        LOGGER.debug("Search for term: {}, storefront: {}, fetchLimit: {}", term, storefront, fetchLimit);

        return musicApiClientProvider.getWebClient().get().uri(uriBuilder -> uriBuilder
                .path("/v1/catalog/" + setDefaultIfNone(storefront) + "/search")
                .queryParam("term", term)
                .queryParam("types", "songs")
                .queryParam("limit", fetchLimit)
                .build()).retrieve()
                .bodyToFlux(MusicApiResults.class)
                .retryWhen(Retry.backoff(10, Duration.ofSeconds(5))
                        .filter(e -> e instanceof WebClientResponseException)
                        .doAfterRetry(value -> LOGGER.info("Retry search for term: {}", term))
                )
                .filter(response -> response.getResults().getSongs() != null)
                .flatMap(response ->
                        Flux.fromIterable(response.getResults().getSongs().getData().stream().map(data -> {
                            MusicAPISong song = data.getAttributes();
                            song.setId(data.getId());
                            return song;
                        }).collect(Collectors.toCollection(ArrayList::new)))
                );
    }
}
