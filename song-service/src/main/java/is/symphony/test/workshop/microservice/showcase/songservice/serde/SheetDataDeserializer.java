package is.symphony.test.workshop.microservice.showcase.songservice.serde;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import org.springframework.kafka.support.serializer.JsonDeserializer;

public class SheetDataDeserializer extends JsonDeserializer<SheetSong> {
}