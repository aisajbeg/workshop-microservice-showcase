package is.symphony.test.workshop.microservice.showcase.songservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SongMatches;
import is.symphony.test.workshop.microservice.showcase.songservice.properties.SongProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Service
public class SongMatchesKafkaProducerService {
    private final Logger LOGGER = LoggerFactory.getLogger(SongMatchesKafkaProducerService.class);

    private final KafkaTemplate<String, SongMatches> songMatchesKafkaTemplate;
    private final SongProperties songProperties;

    public SongMatchesKafkaProducerService(KafkaTemplate<String, SongMatches> songMatchesKafkaTemplate,
                                           SongProperties songProperties) {
        this.songMatchesKafkaTemplate = songMatchesKafkaTemplate;
        this.songProperties = songProperties;
    }

    public Mono<SendResult<String, SongMatches>> send(SongMatches songMatches) throws ExecutionException, InterruptedException {
        LOGGER.info("send to topic={}, {}={},", songProperties.getSongMatchesKafkaTopic(), SongMatches.class.getSimpleName(), songMatches);

        return Mono.just(songMatchesKafkaTemplate.send(songProperties.getSongMatchesKafkaTopic(), songMatches).get());
    }
}
