package is.symphony.test.workshop.microservice.showcase.songservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;

import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;


import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumerConfig.class);

    private final KafkaProperties properties;

    public KafkaConsumerConfig(KafkaProperties properties) {
        this.properties = properties;
    }

    @Bean
    public ConsumerFactory<String, SheetSong> sheetDataConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers().get(0));
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        JsonDeserializer<SheetSong> sheetDataJsonDeserializer = new JsonDeserializer<>(SheetSong.class);
        sheetDataJsonDeserializer.addTrustedPackages("is.symphony.test.workshop.microservice.showcase.generated.model");
        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(),
                sheetDataJsonDeserializer);
    }

    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, SheetSong>> sheetDataKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, SheetSong> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(sheetDataConsumerFactory());
        factory.setConcurrency(5);
        factory.getContainerProperties().setPollTimeout(1000);
        return factory;
    }
}
