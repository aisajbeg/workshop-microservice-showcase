package is.symphony.test.workshop.microservice.showcase.songservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("music.api")
public class MusicAPIProperties {
    private String url;
    private String token;
    private String secret;

    private String kid;

    private String teamId;

    private String password;

    private String salt;

    public String getUrl() {
        return url;
    }

    public MusicAPIProperties setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getToken() {
        return token;
    }

    public MusicAPIProperties setToken(String token) {
        this.token = token;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public MusicAPIProperties setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    public String getKid() {
        return kid;
    }

    public MusicAPIProperties setKid(String kid) {
        this.kid = kid;
        return this;
    }

    public String getTeamId() {
        return teamId;
    }

    public MusicAPIProperties setTeamId(String teamId) {
        this.teamId = teamId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public MusicAPIProperties setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getSalt() {
        return salt;
    }

    public MusicAPIProperties setSalt(String salt) {
        this.salt = salt;
        return this;
    }
}
