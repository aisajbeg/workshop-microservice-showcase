package is.symphony.test.workshop.microservice.showcase.songservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("equivalency.api")
public class EquivalencyApiProperties {
    private String url;

    public String getUrl() {
        return url;
    }

    public EquivalencyApiProperties setUrl(String url) {
        this.url = url;
        return this;
    }
}
