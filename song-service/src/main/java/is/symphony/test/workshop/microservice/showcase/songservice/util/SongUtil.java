package is.symphony.test.workshop.microservice.showcase.songservice.util;

import is.symphony.test.workshop.microservice.showcase.generated.model.MusicAPISong;
import is.symphony.test.workshop.microservice.showcase.generated.model.Song;
import is.symphony.test.workshop.microservice.showcase.generated.model.SongArtwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeParseException;

public class SongUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(SongUtil.class);

    public static final String DEFAULT_STOREFRONT = "US";
    public static final String STOREFRONT_US = "US";
    public static final String STOREFRONT_GB = "GB";
    public static final int DEFAULT_FETCH_LIMIT = 5;

    private SongUtil() {}

    public static String setDefaultIfNone(String storefront) {
        if (storefront == null || storefront.isBlank()) return DEFAULT_STOREFRONT;
        return storefront;
    }

    public static Song songConverter(MusicAPISong song) {
        return new Song()
                .id(song.getId())
                .albumName(song.getAlbumName())
                .artistName(song.getArtistName())
                .name(song.getName())
                .contentRating(song.getContentRating())
                .durationInMillis(song.getDurationInMillis())
                .releaseDate(parseReleaseDate(song.getReleaseDate()))
                .trackNumber(song.getTrackNumber())
                .url(song.getUrl())
                .genreNames(song.getGenreNames())
                .artwork(song.getArtwork() == null ? null :
                        new SongArtwork()
                                .height(song.getArtwork().getHeight())
                                .width(song.getArtwork().getHeight())
                                .url(song.getArtwork().getUrl())
                );
    }

    public static LocalDate parseReleaseDate(String releaseDate) {
        if(releaseDate != null) {
            try {
                return LocalDate.parse(releaseDate);
            } catch (DateTimeParseException e) {
                try {
                    return YearMonth.parse(releaseDate).atDay(1);
                } catch (DateTimeParseException e1) {
                    try {
                        return Year.parse(releaseDate).atMonth(1).atDay(1);
                    } catch (DateTimeParseException e2) {
                        LOGGER.error("Cannot parse {} as release date.", releaseDate);
                    }
                }
            }
        }

        return null;
    }
}
