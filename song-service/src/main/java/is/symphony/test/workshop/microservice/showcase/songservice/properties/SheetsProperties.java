package is.symphony.test.workshop.microservice.showcase.songservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("sheets")
public class SheetsProperties {
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public SheetsProperties setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }
}
