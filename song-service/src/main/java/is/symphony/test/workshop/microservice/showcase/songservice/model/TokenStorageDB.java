package is.symphony.test.workshop.microservice.showcase.songservice.model;

import java.time.LocalDateTime;
import java.util.Objects;

import is.symphony.test.workshop.microservice.showcase.generated.model.Token;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.security.crypto.encrypt.TextEncryptor;

@Table(TokenStorageDB.TABLE_NAME)
public class TokenStorageDB {
    public static final String TABLE_NAME = "tokenstorage";

    public static final String API_NAME = "api_name";

    @PrimaryKeyColumn(
            name = API_NAME,
            ordinal = 0,
            type = PrimaryKeyType.PARTITIONED)
    private String apiName;

    @Column("encr_token")
    private String token;

    @Column("timestamp")
    private LocalDateTime timestamp;

    @Column("expire_date_time")
    private LocalDateTime expireDateTime;

    public String getApiName() {
        return apiName;
    }

    public TokenStorageDB setApiName(String apiName) {
        this.apiName = apiName;
        return this;
    }

    public String getToken() {
        return token;
    }

    public TokenStorageDB setToken(String token) {
        this.token = token;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public TokenStorageDB setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public LocalDateTime getExpireDateTime() {
        return expireDateTime;
    }

    public TokenStorageDB setExpireDateTime(LocalDateTime expireDateTime) {
        this.expireDateTime = expireDateTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenStorageDB that = (TokenStorageDB) o;
        return Objects.equals(apiName, that.apiName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(apiName);
    }

    @Override
    public String toString() {
        return "TokenStorageDB{" +
                "apiName='" + apiName + '\'' +
                ", token='" + token + '\'' +
                ", timestamp=" + timestamp +
                ", expireDateTime=" + expireDateTime +
                '}';
    }

    public Token getDTO(TextEncryptor textEncryptor) {
        return new Token()
                .name(apiName)

                .value(textEncryptor.decrypt(token));
    }

}