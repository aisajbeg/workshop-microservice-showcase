package is.symphony.test.workshop.microservice.showcase.songservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.songservice.service.SongSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Service
public class SheetDataConsumerService {
    private final Logger LOGGER = LoggerFactory.getLogger(SheetDataConsumerService.class);

    private final SongSearchService songSearchService;

    public SheetDataConsumerService(final SongSearchService songSearchService) {
        this.songSearchService = songSearchService;
    }

    @KafkaListener(topics = "${sheets.sheetDataKafkaTopic}", groupId = "${sheets.groupName}", containerFactory = "sheetDataKafkaListenerContainerFactory")
    public void consumeSheetData(SheetSong sheetSong) {
        String songId = sheetSong.getSongId();
        String songName = sheetSong.getSongName();

        LOGGER.info("SongID: {}, SongName: {}", songId, songName);

        songSearchService
                .searchSongs(songId == null || songId.isBlank() ? songName : songId, null, 5)
                .collectList()
                .flatMap(e -> {
                    try {
                        return songSearchService.createTask(e, sheetSong);
                    } catch (ExecutionException executionException) {
                        LOGGER.info("Error!", executionException);
                        return Mono.empty();
                    } catch (InterruptedException interruptedException) {
                        LOGGER.info("Error!", interruptedException);
                        return Mono.empty();
                    }
                })
                .subscribe();
    }
}
