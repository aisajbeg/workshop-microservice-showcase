package is.symphony.test.workshop.microservice.showcase.songservice.configuration;

import is.symphony.test.workshop.microservice.showcase.songservice.model.TokenStorageDB;
import is.symphony.test.workshop.microservice.showcase.songservice.properties.MusicAPIProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Set;

@Configuration
@EnableScheduling
@ComponentScan({
        "is.symphony.test.workshop.microservice.showcase.songservice.properties",
        "is.symphony.test.workshop.microservice.showcase.songservice.handler",
        "is.symphony.test.workshop.microservice.showcase.songservice.serde"
})
public class MusicApiConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(MusicApiConfiguration.class);

    private final MusicAPIProperties musicApiProperties;
    private final WebClient.Builder webClientBuilder;
    private final Environment env;
    private MusicApiClientProvider provider;

    public MusicApiConfiguration(final MusicAPIProperties musicApiProperties,
                                 final WebClient.Builder webClientBuilder,
                                 final Environment env) {
        this.musicApiProperties = musicApiProperties;
        this.webClientBuilder = webClientBuilder;
        this.env = env;
    }

    @Bean
    public TextEncryptor textEncryptor() {
        return Encryptors.text(musicApiProperties.getPassword(), musicApiProperties.getSalt());
    }

    public Mono<String> getBearerToken() {
        /**  TODO: DELETE
        LOGGER.info("Checking db for token...");
        return getToken()
                .flatMap(tokenStorageDB ->
                        Mono.just(textEncryptor().decrypt(tokenStorageDB.getToken())));
         */


        if (Set.of(env.getActiveProfiles()).contains("local")) {
            LOGGER.info("Using predefined token (local run)...");
            LOGGER.info("token local: {}", musicApiProperties.getToken());
            return Mono.just(musicApiProperties.getToken());
        } else {
            LOGGER.info("Checking db for token...");
            return getToken()
                    .flatMap(tokenStorageDB ->
                            Mono.just(textEncryptor().decrypt(tokenStorageDB.getToken())));
        }


    }

    public Mono<TokenStorageDB> getToken() {
        /**
        return tokenStorageRepository.findByApiName(MUSIC_API_APP_NAME)
                .flatMap(token -> {
                    if (ChronoUnit.MONTHS.between(token.getTimestamp(), LocalDateTime.now()) > 4) {
                        return updateTokenForMusicAPI(Mono.just(token));
                    }

                    return Mono.just(token);
                })
                .switchIfEmpty(Mono.defer(this::storeNewToken));

         */
        return Mono.just(new TokenStorageDB().setToken("0f478e3a774b2ebe5209da5d4c809689d84db3946e85be597d7d32d810a6f0506ccbcd858bd5b76f45850bb266e752c230c1617da2dfbcc179c8a4074dcb7b519f2e20c8bc811f0bf3347b188504ed28124917209901920328350afd4d844f50569b0a7033e972caeab452598b3a097a0f03ea9c69d85509e344d395ee5d0e5fed826fd9da6f6623d93dd121c3a0ee6e6522c46fad333f90d1e1dea0a6176ce50aaed972e0dad0ef1a152181d786773070125b95f2bfef9d192535093f77a3f935ce78a226beb3568626e2825662f033aa865079003b0078cdf19c74184c30c9"));
    }

    @Bean
    public MusicApiClientProvider musicApiClientProvider() {
        if(musicApiProperties.getUrl() == null) {
            return null;
        }

        provider = new MusicApiClientProvider()
                .setWebClient(webClientBuilder
                        .baseUrl(musicApiProperties.getUrl())
                        .build()
                );

        getBearerToken().flatMap(token -> {
            LOGGER.info("token: {}", token);    // TODO: DELETE
            provider.updateAuthToken(token);
            return Mono.just(token);
        }).subscribe();

        return provider;
    }

    public static class MusicApiClientProvider {
        private WebClient webClient;

        public WebClient getWebClient() {
            return webClient;
        }

        public MusicApiClientProvider setWebClient(WebClient webClient) {
            this.webClient = webClient;
            return this;
        }

        public void updateAuthToken(String token) {
            LOGGER.info("Adding token to Music API web client...");
            webClient = webClient.mutate().defaultHeaders(header -> {
                if (header.get(HttpHeaders.AUTHORIZATION) == null) {
                    header.add(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token));
                } else {
                    header.set(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token));
                }
            }).build();
        }
    }
}
