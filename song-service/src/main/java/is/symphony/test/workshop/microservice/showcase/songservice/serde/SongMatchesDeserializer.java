package is.symphony.test.workshop.microservice.showcase.songservice.serde;

import is.symphony.test.workshop.microservice.showcase.generated.model.SongMatches;
import org.springframework.kafka.support.serializer.JsonDeserializer;

public class SongMatchesDeserializer extends JsonDeserializer<SongMatches> {
}
