package is.symphony.test.workshop.microservice.showcase.songservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("song")
public class SongProperties {
    private String songMatchesKafkaTopic;
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public SongProperties setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public String getSongMatchesKafkaTopic() {
        return songMatchesKafkaTopic;
    }

    public SongProperties setSongMatchesKafkaTopic(String songMatchesKafkaTopic) {
        this.songMatchesKafkaTopic = songMatchesKafkaTopic;
        return this;
    }
}