package is.symphony.test.workshop.microservice.showcase.taskservice.service;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.generated.model.SongMatches;
import is.symphony.test.workshop.microservice.showcase.generated.model.TaskInfo;
import is.symphony.test.workshop.microservice.showcase.generated.model.TaskStatus;
import is.symphony.test.workshop.microservice.showcase.taskservice.exceptions.TaskCannotBeUpdatedException;
import is.symphony.test.workshop.microservice.showcase.taskservice.model.MatchedOriginalSongDBType;
import is.symphony.test.workshop.microservice.showcase.taskservice.model.TaskDB;
import is.symphony.test.workshop.microservice.showcase.taskservice.properties.TaskProperties;
import is.symphony.test.workshop.microservice.showcase.taskservice.repository.TaskRepository;
import is.symphony.test.workshop.microservice.showcase.taskservice.utility.SongUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.LocalDate;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static is.symphony.test.workshop.microservice.showcase.taskservice.utility.TaskUtility.getShowIdFor;
import static org.apache.commons.lang.StringUtils.isNotBlank;

@Service
public class CreateTaskService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTaskService.class);

    private final TaskRepository taskRepository;
    private final KafkaTemplate<String, SheetSong> sheetSongUpdateKafkaTemplate;
    private final KafkaTemplate<String, TaskInfo> lyricsRequestKafkaTemplate;
    private final TaskProperties taskProperties;

    public CreateTaskService(final TaskRepository taskRepository,
                             final KafkaTemplate<String, SheetSong> sheetSongUpdateKafkaTemplate,
                             KafkaTemplate<String, TaskInfo> lyricsRequestKafkaTemplate,
                             final TaskProperties taskProperties) {
        this.taskRepository = taskRepository;
        this.sheetSongUpdateKafkaTemplate = sheetSongUpdateKafkaTemplate;
        this.lyricsRequestKafkaTemplate = lyricsRequestKafkaTemplate;
        this.taskProperties = taskProperties;
    }

    public Mono<TaskDB> createTask(SongMatches songMatches) {

        String showId = getShowIdFor(LocalDate.now());
        String songId = songMatches.getInputSong().getSongId();

        LOGGER.info("Create task request for {} - {} - {}", showId, songId, songId == null || songId.isBlank() ? UUID.randomUUID() : UUID.nameUUIDFromBytes(songId.getBytes()));

        Mono<TaskDB> existingTask = songId != null && !songId.isBlank() ?
                taskRepository.findById(showId, UUID.nameUUIDFromBytes(songId.getBytes())) : Mono.justOrEmpty((TaskDB) null);

        return existingTask
                .flatMap(
                        // If task has status different than PENDING, throw exception, it should not be updated
                        t -> {
                            if (t.getStatus() != TaskStatus.PENDING)
                                return Mono.error(new TaskCannotBeUpdatedException(t));

                            return Mono.just(t);
                        }
                )
                .switchIfEmpty(getDataForGivenSearchedSong(showId, songId, songMatches.getInputSong()))
                .flatMap(task -> updateTask(task, songId, songMatches))
                .flatMap(taskRepository::save)
                .retryWhen(Retry.max(100).filter(e -> e instanceof OptimisticLockingFailureException))
                .flatMap(e -> {
                    LOGGER.info("Saved: {}", e);
                    return Mono.just(e);
                })
                .onErrorResume(TaskCannotBeUpdatedException.class, e -> {
                    LOGGER.debug("Only tasks with PENDING status can be updated. Task status is: {}.",
                        e.getTask().getStatus());
                    return existingTask;
                })
                .doOnError(e -> LOGGER.error("Error while processing task creation."))
                .doOnNext(e -> {
                    updateSheet(e, songMatches);
                    getLyrics(e);
                });

    }

    private void getLyrics(TaskDB taskDB) {
        if (taskDB.getAdamId() != null) {
            LOGGER.info("Requesting lyrics for {}", taskDB.getTaskInfo());
            lyricsRequestKafkaTemplate.send(taskProperties.getLyricsRequestKafkaTopic(), taskDB.getTaskInfo());
        }
    }

    private void updateSheet(TaskDB taskDB, SongMatches songMatches) {
        SheetSong inputSong = songMatches.getInputSong();
        inputSong.setShowId(taskDB.getShowId());
        inputSong.setStatus(taskDB.getStatus().toString());

        LOGGER.info("Sending updated inputSong {}", inputSong);

        try {
            sheetSongUpdateKafkaTemplate.send(taskProperties.getSheetSongUpdateKafkaTopic(), inputSong).get();
        } catch (InterruptedException interruptedException) {
            LOGGER.error("Issue sending update to sheet service ", interruptedException);
        } catch (ExecutionException executionException) {
            LOGGER.error("Issue sending update to sheet service ", executionException);
        }
    }

    Mono<TaskDB> getDataForGivenSearchedSong(String showId, String songId, SheetSong sheetSong) {
        TaskDB taskDB = new TaskDB()
                .setMatchedClean(false)
                .setMatchedOriginal(isNotBlank(songId))
                .setShowId(showId)
                .setStatus(TaskStatus.PENDING)
                // link task with songMatches data so that we can do updates from same channel and have no duplicates
                .setId(sheetSong.getId());

        return Mono.just(taskDB);
    }

    private Mono<TaskDB> updateTask(TaskDB task, String songId, SongMatches songMatches) {
        Long adamId = getSongId(songId);

        return Mono.just(
                task.setOriginalSongs(SongUtility.convertMatchingSongsToMatchingSongsDB(songMatches.getMatchedSongs()))
                        .setMatchedOriginal(songMatches.getMatchedSongs() != null &&
                                songMatches.getMatchedSongs().size() == 1 &&
                                songMatches.getMatchedSongs().get(0).getId().equals(adamId))
                        .setMatchedOriginalSongDBType(task.isMatchedOriginal() ?
                                new MatchedOriginalSongDBType(songMatches.getMatchedSongs().get(0)) : null)
                        // when due date is null, it goes to bottom of the list
                        .setSheetAdamId(adamId)
        );
    }

    private Long getSongId(String songId) {
        if(isNotBlank(songId)) {
            try {
                return Long.valueOf(songId);
            } catch (NumberFormatException e) {
                LOGGER.error("Error while parsing AdamId: {}. Continue with null.", songId, e);
            }
        }
        return null;
    }
}
