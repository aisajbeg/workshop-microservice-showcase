package is.symphony.test.workshop.microservice.showcase.taskservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SongMatches;
import is.symphony.test.workshop.microservice.showcase.taskservice.service.CreateTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class SongMatchesKafkaConsumerService {
    private final Logger LOGGER = LoggerFactory.getLogger(SongMatchesKafkaConsumerService.class);

    private final CreateTaskService createTaskService;

    public SongMatchesKafkaConsumerService(final CreateTaskService createTaskService) {
        this.createTaskService = createTaskService;
    }

    @KafkaListener(topics = "${song.songMatchesKafkaTopic}", groupId = "${song.groupName}", containerFactory = "songMatchesKafkaListenerContainerFactory")
    public void consumeSongMatchesTaskRequest(SongMatches songMatches) {
        LOGGER.info("Received songMatches to create new task: {}", songMatches);

        createTaskService.createTask(songMatches)
                .flatMap(e -> {
                    LOGGER.info("Stored taskDB: {}", e);
                    return Mono.just(e);
                }).subscribe();
    }
}
