package is.symphony.test.workshop.microservice.showcase.taskservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.TaskInfo;
import is.symphony.test.workshop.microservice.showcase.taskservice.properties.TaskProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Service
public class LyricsRequestKafkaProducerService {
    private final Logger LOGGER = LoggerFactory.getLogger(LyricsRequestKafkaProducerService.class);
    private final KafkaTemplate<String, TaskInfo> lyricsRequestKafkaTemplate;
    private final TaskProperties taskProperties;

    public LyricsRequestKafkaProducerService(KafkaTemplate<String, TaskInfo> lyricsRequestKafkaTemplate, TaskProperties taskProperties) {
        this.lyricsRequestKafkaTemplate = lyricsRequestKafkaTemplate;
        this.taskProperties = taskProperties;
    }

    public Mono<SendResult<String, TaskInfo>> send(TaskInfo taskInfo) throws ExecutionException, InterruptedException {
        LOGGER.info("lyrics requested to topic={}, {}={},", taskProperties.getLyricsRequestKafkaTopic(), TaskInfo.class.getSimpleName(), taskInfo);

        return Mono.just(lyricsRequestKafkaTemplate.send(taskProperties.getLyricsRequestKafkaTopic(), taskInfo).get());
    }
}
