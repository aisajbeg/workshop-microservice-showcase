package is.symphony.test.workshop.microservice.showcase.taskservice.utility;

import is.symphony.test.workshop.microservice.showcase.generated.model.MusicAPISong;
import is.symphony.test.workshop.microservice.showcase.taskservice.model.MatchedOriginalSongDBType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SongUtility {
    private static final Logger LOGGER = LoggerFactory.getLogger(SongUtility.class);

    private SongUtility() {}

    public static LocalDate parseReleaseDate(String releaseDate) {
        if(releaseDate != null) {
            try {
                return LocalDate.parse(releaseDate);
            } catch (DateTimeParseException e) {
                try {
                    return YearMonth.parse(releaseDate).atDay(1);
                } catch (DateTimeParseException e1) {
                    try {
                        return Year.parse(releaseDate).atMonth(1).atDay(1);
                    } catch (DateTimeParseException e2) {
                        LOGGER.error("Cannot parse {} as release date.", releaseDate);
                    }
                }
            }
        }

        return null;
    }

    public static Set<MatchedOriginalSongDBType> convertMatchingSongsToMatchingSongsDB(List<MusicAPISong> matchingSongs) {
        return (matchingSongs == null ||
                matchingSongs.isEmpty()) ?
                Collections.emptySet() :
                matchingSongs.stream()
                        .map(MatchedOriginalSongDBType::new)
                        .collect(Collectors.toSet());
    }
}
