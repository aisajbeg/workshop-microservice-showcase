package is.symphony.test.workshop.microservice.showcase.taskservice.repository;

import is.symphony.test.workshop.microservice.showcase.taskservice.model.TaskDB;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface TaskRepository extends ReactiveCassandraRepository<TaskDB, UUID> {
    @Query("SELECT * FROM " + TaskDB.TABLE_NAME + " WHERE " + TaskDB.SHOW_ID + "=?0 AND " + TaskDB.ID + "=?1")
    Mono<TaskDB> findById(String showId, UUID id);

    @Query("SELECT * FROM " + TaskDB.TABLE_NAME + " WHERE " + TaskDB.SHOW_ID + "=?0")
    Flux<TaskDB> findShowTasks(String showId);
}
