package is.symphony.test.workshop.microservice.showcase.taskservice.utility;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

import static is.symphony.test.workshop.microservice.showcase.taskservice.properties.TaskProperties.DEFAULT_REFERENCE_SHOW_STARTING_DATE;

public class TaskUtility {
    /**
     * Generate 14 days long show id for provided date
     * @param date - date for which show id needs to be calculated
     * @return show id in format of two weeks:  2020-09-21:2020-10-04
     *
     * For example, if given referenceDate is 2020-09-29, which is Tuesday, starting showId will be counted
     * from 2020-09-28, which is Monday, and if date provided is 2021-05-07, showId that will be returned
     * is 2021-04-26:2021-05-09
     *
     */
    public static String getShowIdFor(LocalDate date) {
        return ChronoUnit.DAYS.between(
                DEFAULT_REFERENCE_SHOW_STARTING_DATE.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)),
                date) % 14 > 6 ?
                getShowIdWithPreviousWeek(date) :
                getShowIdWithNextWeek(date);
    }

    static String getShowIdWithPreviousWeek(LocalDate date) {
        return date.minusDays(7).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).toString()
                .concat(":")
                .concat(date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)).toString());
    }

    static String getShowIdWithNextWeek(LocalDate date) {
        return date.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).toString()
                .concat(":")
                .concat(date.plusDays(7).with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)).toString());
    }
}
