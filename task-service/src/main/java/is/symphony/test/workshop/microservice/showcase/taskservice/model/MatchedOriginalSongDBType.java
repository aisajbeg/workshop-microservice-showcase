package is.symphony.test.workshop.microservice.showcase.taskservice.model;

import is.symphony.test.workshop.microservice.showcase.generated.model.MusicAPISong;
import is.symphony.test.workshop.microservice.showcase.generated.model.OriginalSong;
import is.symphony.test.workshop.microservice.showcase.taskservice.utility.SongUtility;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@UserDefinedType("MatchedOriginalSong")
public class MatchedOriginalSongDBType {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MMM dd, yyyy");

    private Long adamID;
    private String name;
    private String albumName;
    private List<String> artists;
    private LocalDate releaseDate;
    private String url;
    private Long durationInMillis;
    private String contentRating;
    private List<String> genreNames;
    private UUID lyricsId;

    public MatchedOriginalSongDBType() {
    }

    public MatchedOriginalSongDBType(MusicAPISong song) {
        adamID = song.getId();
        name = song.getName();
        albumName = song.getAlbumName();
        artists = song.getArtistName() == null ? Collections.emptyList() : Collections.singletonList(song.getArtistName());
        releaseDate = SongUtility.parseReleaseDate(song.getReleaseDate());
        url = song.getUrl();
        durationInMillis = song.getDurationInMillis();
        contentRating = song.getContentRating();
        genreNames = song.getGenreNames();
    }

    public MatchedOriginalSongDBType(OriginalSong song) {
        adamID = song.getAdamID();
        name = song.getName();
        albumName = song.getAlbumName();
        artists = song.getArtists() == null ? Collections.emptyList() : song.getArtists();
        releaseDate = LocalDate.parse(song.getReleaseDate());
        url = song.getUrl();
        durationInMillis = song.getDurationInMillis();
        contentRating = song.getContentRating();
        genreNames = song.getGenreNames();
    }

    public String getName() {
        return name;
    }

    public MatchedOriginalSongDBType setName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getArtists() {
        return artists;
    }

    public MatchedOriginalSongDBType setArtists(List<String> artists) {
        this.artists = artists;
        return this;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public MatchedOriginalSongDBType setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public Long getAdamID() {
        return adamID;
    }

    public MatchedOriginalSongDBType setAdamID(Long adamID) {
        this.adamID = adamID;
        return this;
    }

    public String getAlbumName() {
        return albumName;
    }

    public MatchedOriginalSongDBType setAlbumName(String albumName) {
        this.albumName = albumName;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public MatchedOriginalSongDBType setUrl(String url) {
        this.url = url;
        return this;
    }

    public Long getDurationInMillis() {
        return durationInMillis;
    }

    public MatchedOriginalSongDBType setDurationInMillis(Long durationInMillis) {
        this.durationInMillis = durationInMillis;
        return this;
    }


    public String getContentRating() {
        return contentRating;
    }

    public MatchedOriginalSongDBType setContentRating(String contentRating) {
        this.contentRating = contentRating;
        return this;
    }

    public List<String> getGenreNames() {
        return genreNames;
    }

    public MatchedOriginalSongDBType setGenreNames(List<String> genreNames) {
        this.genreNames = genreNames;
        return this;
    }

    public UUID getLyricsId() {
        return lyricsId;
    }

    public MatchedOriginalSongDBType setLyrics(UUID lyricsId) {
        this.lyricsId = lyricsId;
        return this;
    }

    @Override
    public String toString() {
        return "MatchedOriginalSongDBType{" +
                "adamID=" + adamID +
                ", name='" + name + '\'' +
                ", albumName='" + albumName + '\'' +
                ", artists=" + artists +
                ", releaseDate=" + releaseDate +
                ", url='" + url + '\'' +
                ", durationInMillis=" + durationInMillis +
                ", contentRating='" + contentRating + '\'' +
                ", genreNames=" + genreNames +
                ", lyricsId=" + lyricsId +
                '}';
    }

    public OriginalSong toDTO() {
        return new OriginalSong()
                .adamID(adamID)
                .name(name)
                .artists(artists)
                .albumName(albumName)
                .durationInMillis(durationInMillis)
                .releaseDate(releaseDate != null ? DATE_FORMATTER.format(releaseDate) : null)
                .contentRating(contentRating)
                .genreNames(genreNames)
                .lyricsId(lyricsId);
    }
}
