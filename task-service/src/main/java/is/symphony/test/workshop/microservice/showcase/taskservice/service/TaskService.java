package is.symphony.test.workshop.microservice.showcase.taskservice.service;

import is.symphony.test.workshop.microservice.showcase.generated.model.Task;
import is.symphony.test.workshop.microservice.showcase.taskservice.model.TaskDB;
import is.symphony.test.workshop.microservice.showcase.taskservice.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.UUID;

@Service
public class TaskService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskService.class);

    private final TaskRepository taskRepository;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Flux<Task> findTasks(String showId, List<UUID> filterIds) {
        LOGGER.info("finding all tasks for show: {}", showId);

        if (filterIds != null) {
            LOGGER.info("Filtering all task for ids: {}", filterIds);
        }

        return taskRepository
                .findShowTasks(showId)
                .filter(task -> filterIds == null || filterIds.contains(task.getId()))
                .map(TaskDB::getDTO);
      //          .sort(Comparators.compareTasks);
    }
}
