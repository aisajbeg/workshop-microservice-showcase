package is.symphony.test.workshop.microservice.showcase.taskservice.controller;

import is.symphony.test.workshop.microservice.showcase.taskservice.generated.api.TasksApi;
import is.symphony.test.workshop.microservice.showcase.generated.model.Task;
import is.symphony.test.workshop.microservice.showcase.taskservice.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static is.symphony.test.workshop.microservice.showcase.taskservice.utility.TaskUtility.getShowIdFor;

@RestController
public class TaskController implements TasksApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(value = "/tasks/current",
            produces = { "application/json" },
            method = RequestMethod.GET)
    @Override
    public Mono<ResponseEntity<Flux<Task>>> getTasksForCurrentShow(@Valid @RequestParam(value = "ids", required = false) List<UUID> ids,
                                                                   ServerWebExchange exchange) {

        LOGGER.info("Task Service getTasksForCurrentShow called with ids {}", ids);

        String showId = getShowIdFor(LocalDate.now());

        return Mono.just(ResponseEntity.ok(taskService.findTasks(showId, ids)));
    }
}
