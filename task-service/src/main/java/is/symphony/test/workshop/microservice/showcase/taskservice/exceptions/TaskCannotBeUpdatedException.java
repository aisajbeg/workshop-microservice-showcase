package is.symphony.test.workshop.microservice.showcase.taskservice.exceptions;

import is.symphony.test.workshop.microservice.showcase.taskservice.model.TaskDB;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TaskCannotBeUpdatedException extends ResponseStatusException {
    private TaskDB task;

    public TaskCannotBeUpdatedException(TaskDB task) {
        super(HttpStatus.CONFLICT, "Task cannot be updated. Only tasks with PENDING status can be updated.");

        this.task = task;
    }

    public TaskDB getTask() {
        return task;
    }
}