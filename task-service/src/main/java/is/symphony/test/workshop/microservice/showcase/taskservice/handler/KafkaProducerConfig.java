package is.symphony.test.workshop.microservice.showcase.taskservice.handler;

import is.symphony.test.workshop.microservice.showcase.generated.model.SheetSong;
import is.symphony.test.workshop.microservice.showcase.generated.model.SongMatches;
import is.symphony.test.workshop.microservice.showcase.generated.model.TaskInfo;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {
    private final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerConfig.class);

    private final KafkaProperties kafkaProperties;

    public KafkaProducerConfig(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }


    @Bean
    public KafkaTemplate<String, SheetSong> sheetSongUpdateKafkaTemplate() {
        return new KafkaTemplate<>(sheetSongUpdateProducerFactory());
    }

    @Bean
    public ProducerFactory<String, SheetSong> sheetSongUpdateProducerFactory() {
        Map<String, Object> configProps = new HashMap<>(6);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers().get(0));
        configProps.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, TaskInfo> lyricsRequestKafkaTemplate() {
        return new KafkaTemplate<>(lyricsRequestProducerFactory());
    }

    @Bean
    public ProducerFactory<String, TaskInfo> lyricsRequestProducerFactory() {
        Map<String, Object> configProps = new HashMap<>(6);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers().get(0));
        configProps.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return new DefaultKafkaProducerFactory<>(configProps);
    }
}
