package is.symphony.test.workshop.microservice.showcase.taskservice.model;

import is.symphony.test.workshop.microservice.showcase.generated.model.Task;
import is.symphony.test.workshop.microservice.showcase.generated.model.TaskInfo;
import is.symphony.test.workshop.microservice.showcase.generated.model.TaskStatus;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.EMPTY_LIST;

@Table(TaskDB.TABLE_NAME)
public class TaskDB {
    public static final String TABLE_NAME = "Task";

    public static final String SHOW_ID = "show_id";

    public static final String ID = "id";

    public static final String STATUS = "status";

    public static final String MATCHED_ORIGINAL = "matched_original";

    public static final String MATCHED_CLEAN = "matched_clean";

    public static final String ORIGINAL_SONGS = "original_songs";

    public static final String MATCHED_ORIGINAL_SONG = "matched_original_song";

    @Column(MATCHED_ORIGINAL_SONG)
    private MatchedOriginalSongDBType matchedOriginalSongDBType;

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MMM d, yyyy hh:mm a");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MMM dd, yyyy");

    @PrimaryKeyColumn(
            name = ID,
            ordinal = 1,
            type = PrimaryKeyType.CLUSTERED,
            ordering = Ordering.DESCENDING)
    private UUID id;

    @PrimaryKeyColumn(
            name = SHOW_ID,
            ordinal = 0,
            type = PrimaryKeyType.PARTITIONED)
    private String showId;

    @Column("task_artist_names")
    private Set<String> taskArtistNames = new HashSet<>(5);

    private String taskSongName;

    private Long adamId;

    private TaskStatus status;

    @Column(MATCHED_CLEAN)
    private boolean matchedClean;

    @Column(MATCHED_ORIGINAL)
    private boolean matchedOriginal;

    @Column(ORIGINAL_SONGS)
    private Set<MatchedOriginalSongDBType> originalSongs;

    @Column("sheet_adam_id")
    private Long sheetAdamId;

    public UUID getId() {
        return id;
    }

    public TaskDB setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getShowId() {
        return showId;
    }

    public TaskDB setShowId(String showId) {
        this.showId = showId;
        return this;
    }

    public Set<String> getTaskArtistNames() {
        return taskArtistNames;
    }

    public TaskDB setTaskArtistNames(Set<String> taskArtistNames) {
        this.taskArtistNames = taskArtistNames;
        return this;
    }

    public String getTaskSongName() {
        return taskSongName;
    }

    public TaskDB setTaskSongName(String taskSongName) {
        this.taskSongName = taskSongName;
        return this;
    }

    public Long getAdamId() {
        return adamId;
    }

    public TaskDB setAdamId(Long adamId) {
        this.adamId = adamId;
        return this;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public TaskDB setStatus(TaskStatus status) {
        this.status = status;
        return this;
    }

    public boolean isMatchedClean() {
        return matchedClean;
    }

    public TaskDB setMatchedClean(boolean matchedClean) {
        this.matchedClean = matchedClean;
        return this;
    }

    public boolean isMatchedOriginal() {
        return matchedOriginal;
    }

    public TaskDB setMatchedOriginal(boolean matchedOriginal) {
        this.matchedOriginal = matchedOriginal;
        return this;
    }

    public Set<MatchedOriginalSongDBType> getOriginalSongs() {
        return originalSongs;
    }

    public TaskDB setOriginalSongs(Set<MatchedOriginalSongDBType> originalSongs) {
        this.originalSongs = originalSongs;
        return this;
    }

    public MatchedOriginalSongDBType getMatchedOriginalSongDBType() {
        return matchedOriginalSongDBType;
    }

    public TaskDB setMatchedOriginalSongDBType(MatchedOriginalSongDBType matchedOriginalSongDBType) {
        this.matchedOriginalSongDBType = matchedOriginalSongDBType;
        return this;
    }

    public Long getSheetAdamId() {
        return sheetAdamId;
    }

    public TaskDB setSheetAdamId(Long sheetAdamId) {
        this.sheetAdamId = sheetAdamId;
        return this;
    }

    public Task getDTO() {
        return new Task()
                .id(this.id)
                .showId(this.showId)
                .adamId(this.adamId)
                .matchedCleaned(this.matchedClean)
                .matchedOriginal(this.matchedOriginal)
                .taskSongName(this.taskSongName)
                .status(this.status)
                .originalSongs(this.originalSongs == null ? Collections.emptyList() : this.originalSongs.stream().map(MatchedOriginalSongDBType::toDTO).collect(Collectors.toList()))
                .taskArtistNames(this.taskArtistNames == null ? EMPTY_LIST : new ArrayList<>(this.taskArtistNames));
    }

    public TaskInfo getTaskInfo() {
        return new TaskInfo()
                .id(this.id)
                .showId(this.showId)
                .adamId(this.adamId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDB taskDB = (TaskDB) o;
        return Objects.equals(id, taskDB.id) &&
                Objects.equals(showId, taskDB.showId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, showId);
    }

    @Override
    public String toString() {
        return "TaskDB{" +
                "matchedOriginalSongDBType=" + matchedOriginalSongDBType +
                ", id=" + id +
                ", showId='" + showId + '\'' +
                ", taskArtistNames=" + taskArtistNames +
                ", taskSongName='" + taskSongName + '\'' +
                ", adamId=" + adamId +
                ", status=" + status +
                ", matchedClean=" + matchedClean +
                ", matchedOriginal=" + matchedOriginal +
                ", originalSongs=" + originalSongs +
                ", sheetAdamId=" + sheetAdamId +
                '}';
    }
}
