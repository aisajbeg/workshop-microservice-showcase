package is.symphony.test.workshop.microservice.showcase.taskservice.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

@Configuration
@ConfigurationProperties("task")
public class TaskProperties {
    public static LocalDate DEFAULT_REFERENCE_SHOW_STARTING_DATE;

    private List<String> languages;
    private String sheetSongUpdateKafkaTopic;
    private String lyricsRequestKafkaTopic;

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public String getSheetSongUpdateKafkaTopic() {
        return sheetSongUpdateKafkaTopic;
    }

    public TaskProperties setSheetSongUpdateKafkaTopic(String sheetSongUpdateKafkaTopic) {
        this.sheetSongUpdateKafkaTopic = sheetSongUpdateKafkaTopic;
        return this;
    }

    @Value("#{T(java.time.LocalDate).parse('${show.starting.date}', T(java.time.format.DateTimeFormatter).ofPattern('yyyy-MM-dd')) ?: T(java.time.LocalDate).now()}")
    public TaskProperties setDefaultReferenceShowStartingDate(LocalDate defaultReferenceShowStartingDate) {
        DEFAULT_REFERENCE_SHOW_STARTING_DATE = defaultReferenceShowStartingDate;
        return this;
    }

    public String getLyricsRequestKafkaTopic() {
        return lyricsRequestKafkaTopic;
    }

    public TaskProperties setLyricsRequestKafkaTopic(String lyricsRequestKafkaTopic) {
        this.lyricsRequestKafkaTopic = lyricsRequestKafkaTopic;
        return this;
    }
}
