package is.symphony.test.workshop.microservice.showcase.taskservice.model;

import is.symphony.test.workshop.microservice.showcase.generated.model.Artwork;
import is.symphony.test.workshop.microservice.showcase.generated.model.SongArtwork;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

import java.util.Objects;

@UserDefinedType("OriginalSongArtwork")
public class OriginalSongArtworkDBType {
    private Integer height;
    private Integer width;
    private String url;

    public OriginalSongArtworkDBType() {
    }

    public OriginalSongArtworkDBType(SongArtwork artwork) {
        this.height = artwork.getHeight();
        this.width = artwork.getWidth();
        this.url = artwork.getUrl();
    }

    public OriginalSongArtworkDBType(Artwork originalSongArtwork) {
        this.height = originalSongArtwork.getHeight();
        this.width = originalSongArtwork.getWidth();
        this.url = originalSongArtwork.getUrl();
    }

    public Integer getHeight() {
        return height;
    }

    public OriginalSongArtworkDBType setHeight(Integer height) {
        this.height = height;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public OriginalSongArtworkDBType setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public OriginalSongArtworkDBType setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OriginalSongArtworkDBType that = (OriginalSongArtworkDBType) o;
        return Objects.equals(height, that.height) &&
                Objects.equals(width, that.width) &&
                Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, width, url);
    }

    @Override
    public String toString() {
        return "OriginalSongArtworkDBType{" +
                "height=" + height +
                ", width=" + width +
                ", url='" + url + '\'' +
                '}';
    }

    public Artwork toDTO() {
        return new Artwork()
                .height(height)
                .width(width)
                .url(url);
    }
}
